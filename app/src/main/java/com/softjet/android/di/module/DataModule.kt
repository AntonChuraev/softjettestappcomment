package com.softjet.android.di.module

import com.softjet.android.data.api.AppService
import com.softjet.android.data.local.dao.UserDao
import com.softjet.android.data.mappers.UserDetailsToEntityMapper
import com.softjet.android.data.mappers.UsersListToEntityMapper
import com.softjet.android.data.repository.UsersRepositoryImpl
import com.softjet.android.domain.repository.UsersRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DataModule {

    @Singleton
    @Provides
    fun providesUsersRepository(
        appService: AppService,
        usersListToToEntityMapper: UsersListToEntityMapper,
        usersDetailsToEntityMapper: UserDetailsToEntityMapper
    ): UsersRepository = UsersRepositoryImpl(appService, usersListToToEntityMapper, usersDetailsToEntityMapper)
}