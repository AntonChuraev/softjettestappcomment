package com.softjet.android.di.module

import com.softjet.android.di.scope.PerActivity
import com.softjet.android.presentation.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @PerActivity
    @ContributesAndroidInjector
    abstract fun get(): MainActivity
}
