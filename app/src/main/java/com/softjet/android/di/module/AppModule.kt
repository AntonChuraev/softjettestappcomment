package com.softjet.android.di.module

import com.softjet.android.di.module.viewModuile.ViewModelModule
import dagger.Module

@Module(
    includes = [
        ViewModelModule::class,
        NetworkModule::class
    ]
)
class AppModule