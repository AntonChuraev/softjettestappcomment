package com.softjet.android.di.module.viewModuile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.softjet.android.di.factory.ViewModelFactory
import com.softjet.android.presentation.ui.main.users.UsersListViewModel
import com.softjet.android.presentation.ui.main.users.details.UserDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(UsersListViewModel::class)
    internal abstract fun bindUsersListViewModel(viewModel: UsersListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailsViewModel::class)
    internal abstract fun bindUserDetailsViewModel(viewModel: UserDetailsViewModel): ViewModel
}