package com.softjet.android.di.module

import android.content.Context
import com.softjet.android.MainApplication
import com.softjet.android.di.qualifier.ApplicationContext
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    @ApplicationContext
    internal abstract fun application(mainApplication: MainApplication): Context
}
