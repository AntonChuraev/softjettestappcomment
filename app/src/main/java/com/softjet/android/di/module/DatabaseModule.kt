package com.softjet.android.di.module

import android.app.Application
import androidx.room.Room
import com.softjet.android.data.local.AppDatabase
import com.softjet.android.data.local.dao.UserDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    internal fun provideAppDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, AppDatabase.DB_NAME)
                .allowMainThreadQueries().build()

    }

    @Provides
    internal fun provideUserDao(appDatabase: AppDatabase): UserDao {
        return appDatabase.userDao
    }
}