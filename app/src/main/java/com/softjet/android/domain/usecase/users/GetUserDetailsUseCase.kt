package com.softjet.android.domain.usecase.users

import com.softjet.android.data.local.entity.UserEntity
import com.softjet.android.data.local.entity.UsersListEntity
import com.softjet.android.domain.repository.UsersRepository
import com.softjet.android.domain.usecase.base.BackgroundUseCase
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetUserDetailsUseCase @Inject constructor(private val usersRepository: UsersRepository) : BackgroundUseCase() {

    var userId: Long? = null

    override fun buildObservableTask(): Observable<UserEntity> {
        return usersRepository.getUserDetails(userId)
    }
}