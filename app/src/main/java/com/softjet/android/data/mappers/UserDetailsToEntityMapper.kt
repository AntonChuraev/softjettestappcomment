package com.softjet.android.data.mappers

import com.softjet.android.data.local.entity.UserEntity
import com.softjet.android.domain.model.UserDetailsModel
import javax.inject.Inject

class UserDetailsToEntityMapper @Inject constructor() : Mapper<UserDetailsModel, UserEntity>() {
    override fun mapFrom(from: UserDetailsModel): UserEntity {
        val model = from.data
        return UserEntity(
            id = model?.id,
            email = model?.email,
            firstName = model?.firstName,
            lastName = model?.lastName,
            avatar = model?.avatar
        )
    }
}