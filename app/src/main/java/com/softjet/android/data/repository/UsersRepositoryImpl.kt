package com.softjet.android.data.repository

import com.softjet.android.data.api.AppService
import com.softjet.android.data.local.dao.UserDao
import com.softjet.android.data.local.entity.UserEntity
import com.softjet.android.data.local.entity.UsersListEntity
import com.softjet.android.data.mappers.UserDetailsToEntityMapper
import com.softjet.android.data.mappers.UsersListToEntityMapper
import com.softjet.android.domain.repository.UsersRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val appService: AppService,
    private val usersListToEntityMapper: UsersListToEntityMapper,
    private val usersDetailsToEntityMapper: UserDetailsToEntityMapper,
) : UsersRepository {

    override fun getAllUsers(): Observable<UsersListEntity> {
        return appService.getUsers()
            .map { usersListToEntityMapper.mapFrom(it) }
//            .map {
//                userDao.insertAllUsers(it.result!!)
//                it
//            }
    }

    override fun getUserDetails(userId: Long?): Observable<UserEntity> {
        return appService.getUserDetails(userId)
            .map { usersDetailsToEntityMapper.mapFrom(it) }
    }
}