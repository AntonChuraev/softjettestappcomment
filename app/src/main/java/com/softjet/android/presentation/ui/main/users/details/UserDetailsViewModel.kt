package com.softjet.android.presentation.ui.main.users.details

import androidx.lifecycle.MutableLiveData
import com.softjet.android.data.local.entity.UserEntity
import com.softjet.android.domain.usecase.base.DefaultInternetSubscriber
import com.softjet.android.domain.usecase.users.GetUserDetailsUseCase
import com.softjet.android.presentation.ui.base.BaseViewModel
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(private val getUserDetailsUseCase: GetUserDetailsUseCase) : BaseViewModel() {

    val userDetailsLiveData = MutableLiveData<UserEntity>()

    fun getUserDetails(id: Long) {
        getUserDetailsUseCase.userId = id
        addDisposable(getUserDetailsUseCase.execute(object : DefaultInternetSubscriber<UserEntity>() {
            override fun onNext(t: UserEntity) {
                userDetailsLiveData.value = t
            }
        }))
    }
}